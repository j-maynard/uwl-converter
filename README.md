# UWL Number Converter #

## Introduction: ##
This is a simple number converter that convers decimal, hexadecimal and
binary numbers.  The program is written in python and supports a GUI mode,
an interactive mode, a command line mode and bulk conversion mode.

We recommend against running the program in Idle as you'll be unable to
access many of the advanced features the program offers.  If you do run
the program in Idle it'll warn you and only provide you with a subset of
the features the program has to offer.

Requirements:

* Any Mac OS X, Linux or Windows computer
* Python 3.2 or above (tested in python 3.3)
* For the GUI interface you need tkinter installed
* For the Curses interface you need ncurses running on a Mac or BSD/Unix/Linux machine

## Getting Started: ##

Getting started with the converter is as easy as going to the
the command line and typing "./converter".  This will automatically
display the converter help

To start the converter in GUI mode type:

	$ ./converter -g

To start the converter in Interactive console mode type:

	$ ./converter -i

The converter supports quick conversions at the command line by typing
the function you want followed by the number to convert.  You can specify
as many function / number pairs as you want.  e.g.

	$ ./converter --bd 111
	$ ./converter --bd 111 --bh 111 --db=15 --db 304202

The functions are:

| Short | Long             | Name                   |
|-------|------------------|------------------------|
| --db  | --decimal-binary | Decimal to binary      |
| --dh  | --decimal-hex    | Decimal to hexadecimal |
| --bd  | --binary-decimal | Binary to decimal      |
| --bh  | --binary-hex     | Binary to hexadecimal  |
| --hd  | --hex-decimal    | Hex to decimal         |
| --hb  | --hex-binary     | Hex to binary          |

It should be noted that when specifying the each function that you should use
'--' followed by the function name as specifying '-' won't work.

## Additional Files: ##

There are 8 additional files that should be kept with the converter.
These files provide various functions to make the converter run and if
any of them are lost the converter will stop working.

The files included in this package are: 

Required files:

* converter
* bulk.py
* engine.py
* guinteractive.py
* cinteractivemode.py
* interactivemode.py
* typetest.py
* converter.py
	
Optional files:

* README
* User Manual.pdf
* User Manual.doc

## Further Information and Help ##

To get further information first off try looking in the user manual which
came with this program.  If that doesn't help you then please e-mail me at
jamie . maynard @ me . com and I'll try to help in anyway I can.
#!/usr/bin/env python3

# Lets get our modules
import os.path
import sys
from typetest import *
import engine

# This process files with mixed contents
def mixedprocess(numberlist):
	# Create an output buffer
	output = ""
	# For each line in the number list
	# split it and then convert the number
	# according to its function
	for number in numberlist[1:]:
		# first stage... split the function/value pairs
		function,myno = str(number).split(',')
		# Lets take our function and then process it
		# Decimal to Binary
		if function == 'db':
			if isInt(myno):
				myno = engine.dectobin(myno)
		# Decimal to Hex
		if function == 'dh':
			if isInt(myno):
				myno = engine.dectohex(myno)
		# Binary to Decimal
		if function == 'bd':
			if isBin(myno):
				myno = engine.bintodec(myno)
		# Binary to Hex
		if function == 'bh':
			if isBin(myno):
				myno = engine.bintohex(myno)
		# Hex to Decimal
		if function == 'hd':
			if isHex(myno):
				myno = engine.hextodec(myno)
		# Hex to Binary
		if function == 'hb':
			if isHex(myno):
				myno = engine.hextobin(myno)
		# Lets add the conversion to our output buffer
		output = output + myno + "\n"
	# Return our output so we can save it
	return output

# This converts files with only decimal to binaries
def dectobinfile(numberlist):
	output = ""
	for number in numberlist[1:]:
		if isInt(number):
			binno = engine.dectobin(number)
			output = output + binno + "\n"
	return output
	
# This converts files with only decimal to hexs
def dectohexfile(numberlist):
	output = ""
	for number in numberlist[1:]:
		if isInt(number):
			hexno = engine.dectohex(number)
			output = output + hexno + "\n"
	return output
	
# This converts files with only binary to decimals
def bintodecfile(numberlist):
	output = ""
	for number in numberlist[1:]:
		if isBin(number):
			decno = engine.bintodec(number)
			output = output + decno + "\n"
	return output

# This converts files with only binary to hexs
def bintohexfile(numberlist):
	output = ""
	for number in numberlist[1:]:
		if isBin(number):
			hexno = engine.bintohex(number)
			output = output + hexno + "\n"
	return output

# This converts files with only hex to decimals
def hextodecfile(numberlist):
	output = ""
	for number in numberlist[1:]:
		if isHex(number):
			decno = engine.dectohex(number)
			output = output + decno + "\n"
	return output

# This converts files with only hex to binaries
def hextobinfile(numberlist):
	output = ""
	for number in numberlist[1:]:
		if isHex(number):
			binno = engine.hextobin(number)
			output = output + binno + "\n"
	return output

# This function writes our output back to disk
def writefile(output):
	# Ask the user for filename to output to
	outpath = input('Please specify a filename: ')
	# Test to make sure the file isn't there already
	if os.path.isfile(outpath):
		oktowrite = 0
		# Enter this while loop while to give the user the option
		# to specify a different file or to overwrite the existing file
		while oktowrite == 0:
			overwrite = input('File already exists... Overwrite? [y/N] ')
			if overwrite == 'y' or overwrite == 'Y' or overwrite == 'yes' or overwrite == 'YES' or overwrite == 'Yes':
				oktowrite = 1
			else:
				outpath = input('Please specify a different filename: ')
				if os.path.isfile(outpath):
					oktowrite = 0
				else:
					oktowrite = 1
	# Open the file for writing
	writefile = open(outpath, 'w')
	# Write our output buffer to the file
	writefile.write(output)
	# Close the file
	writefile.close()

# This is our main function called from our
# converter bootstrap code.  It takes in a
# file path as an input
def main(inpath):
	# Before we do anything test to see if the file exists
	if not os.path.isfile(inpath):
		# If it doesn't just exit here advising the user
		print('File not found.  Exiting...')
		sys.exit()
	
	# Open the file in readonly mode
	readfile = open(inpath, 'r')
	# Read the file in memory before closing the file
	numberlist = readfile.readlines()
	readfile.close()
	
	# At this point we'll only work on the contents of the file in memory
	# The first line of the file should always contain the function
	# Grab the function and pass the file to the appropriate place
	if numberlist[0] == 'csv\n' or numberlist[0] == 'mixed\n':
		output = mixedprocess(numberlist)
	elif numberlist[0] == 'dectobin\n' or numberlist[0] == 'db\n':
		output = dectobinfile(numberlist)
	elif numberlist[0] == 'dectohex\n' or numberlist[0] == 'dh\n':
		output = dectohexfile(numberlist)
	elif numberlist[0] == 'bintodec\n' or numberlist[0] == 'bd\n':
		output = bintodecfile(numberlist)
	elif numberlist[0] == 'bintohex\n' or numberlist[0] == 'bh\n':
		output = bintohexfile(numberlist)
	elif numberlist[0] == 'hextodec\n' or numberlist[0] == 'hd\n':
		output = hextodecfile(numberlist)
	elif numberlist[0] == 'hextobin\n' or numberlist[0] == 'hb\n':
		output = hextobinfile(numberlist)
		
	# IF we're here it means we might be missing a headerline
	else:
		# Lets try to see if this is a mixed file before
		# we do anything else.  If this errors at any point
		# we'll ask for user intervention
		try:
			# Advise the user what we're doing
			print('No headerline found.  Trying as a mixed format file...')
			# Try to split the first line
			function,myno = str(numberlist[0]).split(',')
			# If we're here it looks like we were sucessful...
			# Add a fake headerline to the file in memory
			numberlist.insert(0,'mixed\n')
			# Process the file
			output = mixedprocess(numberlist)
		#OK we're here... That didn't work 
		except:
			# Advise the user we have a problem
			print('Unknown file type.  File has no headerline')
			print('WARNING - Manual specifying a conversion type may result in incomplete output')
			# Ask for the users intervention
			convert = input('Do you want to specify a conversion type [y/N]: ')
			# Display a menu asking the user to specify the contents of the file
			# and the direction they wish to take
			if convert == 'y' or convert == 'Y' or convert == 'yes' or convert == 'YES' or convert == 'Yes':
				print('\nSpecify a file type:\n')
				print('    1. Decimal to Binary')
				print('    2. Decimal to Hexadecimal')
				print('    3. Binary to Decimal')
				print('    4. Binary to Hexadecimal')
				print('    5. Hexadecimal to Decimal')
				print('    6. Hexadecimal to Binary')
				print('    7. Exit.\n')
				type = input('Specify a file type [1-7]: ')
				rtype = 1
				# This while loop waits for us to get a valid input from the user
				while rtype == 1:
					if isInt(type) and int(type) > 0 and int(type) < 7:
						rtype = 0
					else:
						print('Unknown option. Please choose a number between 1 and 7.')
						type = input('Specify a file type [1-7]: ')
				# Then we send the output to the users chosen function
				# adding on a header line at the start of the output of type unknown
				if type == 1:
					numberlist.insert(0,'unknown\n')
					output = dectobinfile(numberlist)
				elif type == 2:
					numberlist.insert(0,'unknown\n')
					output = dectohexfile(numberlist)
				elif type == 3:
					numberlist.insert(0,'unknown\n')
					output = bintodecfile(numberlist)
				elif type == 4:
					numberlist.insert(0,'unknown\n')
					output = bintohexfile(numberlist)
				elif type == 5:
					numberlist.insert(0,'unknown\n')
					output = hextodecfile(numberlist)
				elif type == 6:
					numberlist.insert(0,'unknown\n')
					output = hextobinfile(numberlist)
			# Other wise the user choose to exit and we exit here		
			else:
				sys.exit(0)
	# Display the converted file to the user
	print('Coverted file:')			
	print(output)
	# Ask the user to write the out buffer to disk
	write = input('Write out to disk? [Y/n]')
	if write == 'n' or write == 'N' or write == 'no' or write == 'NO' or write == 'No':
		# If no exit here
		print('Exiting...')
		sys.exit(0)
	else:
		# Otherwise write file and then exit
		writefile(output)
		sys.exit(0)
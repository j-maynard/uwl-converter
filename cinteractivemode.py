#!/usr/bin/env python3
import curses
import sys
import typetest
import engine

# Code implements switch and case statments and is taken from
# http://stackoverflow.com/questions/60208/replacements-for-switch-statement-in-python
class switch( object ):
    value = None
    def __new__( class_, value ):
        class_.value = value
        return True
# Code implements switch and case statments and is taken from
# http://stackoverflow.com/questions/60208/replacements-for-switch-statement-in-python
def case( *args ):
    return any( ( arg == switch.value for arg in args ) )

# This function is used to get an input from the user
# and return it in a way we can use
def get_text(screen, x, y):
	mytext = screen.getstr(x, y, 15)
	text = str( mytext, encoding='utf8' )
	return text

# This function adds a two line title bar at the top of the screen
# It also centers the text as near as possible on the screen
def subtitle(screen,maintitle,subtitle):
	# Set up the main title
	height,width = screen.getmaxyx()
	hpos = (width // 2) - (len(maintitle) // 2)
	screen.addstr(2, hpos, maintitle)
	# Set up the sub title
	hpos = (width // 2) - (len(subtitle) // 2)
	screen.addstr(3, hpos, subtitle)
	screen.addstr(5, 1, '')
	screen.hline('-', width-2)

# This creates a rudamentry status bar at the bottom of the screen
def statusbar(screen, string):
	height,width = screen.getmaxyx()
	vpos = height - 2
	screen.addstr(vpos, 1, '')
	screen.hline(' ', width-2, curses.A_REVERSE)
	screen.addstr(vpos, 2, string, curses.A_REVERSE)

# This display the results of our conversions on the middle of the
# screen at the same time clearing the background
def convmessage(screen, string):
	height,width = screen.getmaxyx()
	linesize = len(string) + 4
	vpos = (height // 2)
	hpos = (width // 2) - (linesize // 2)
	screen.addstr((vpos-1), hpos, '')
	screen.hline(' ', linesize, curses.A_REVERSE)
	screen.addstr(vpos, hpos, '')
	screen.hline(' ', linesize, curses.A_REVERSE)
	screen.addstr((vpos+1), hpos, '')
	screen.hline(' ', linesize, curses.A_REVERSE)
	screen.addstr(vpos, hpos+2, string, curses.A_REVERSE)

# This handles converting a decimal to a binary number on its own screen
def cdectobin(myscreen):
	running = 1
	optch = 0
	
	# Clear the screen from its previous use
	myscreen.clear()
	myscreen.border(0)
	#Set up the title bar
	subtitle(myscreen, 'UWL Number Converter', 'Decimal to Binary')
	
	# Set up the statusbar
	statusbar(myscreen, 'Type x followed by the return key to return to the main menu')

	# Set up the main screen
	myscreen.addstr(7, 4, "Enter a whole number: ")

	# Get the users input
	mytext = get_text(myscreen,7,35)
	
	# Lets process the users input
	while running == 1:
		# This will capture the user trying to exit to the main menu
		if mytext == 'x' or mytext == 'X':
			myscreen.clear()
			myscreen.border(0)
			running = 0
			return True
		# This handles a valid input from the user
		elif typetest.isInt(mytext):
			decno = mytext
			binno = engine.dectobin(decno)
			mystring = decno + ' converted in to binary is: ' + binno

			#Lets clear the input line and error line to save any confusion
			height,width = myscreen.getmaxyx()
			myscreen.addstr(7, 4, '')
			myscreen.hline(' ', (width-5))
			myscreen.addstr(9, 4, '')
			myscreen.hline(' ', (width-5))

			#Display the result of the conversion and what the user should do next
			convmessage(myscreen, mystring)
			statusbar(myscreen, 'x to exit | 1 to convert another | Press enter to return to the main menu')
			myscreen.refresh()
			running = 0
		# If all else fails advise the users input isn't valid
		# and return back to where we were until we have a valid input
		else:
			myscreen.addstr(7,35, '')
			linelength = 15
			myscreen.hline(' ', linelength)
			myscreen.addstr(9,35,'Enter a valid number or x to exit', curses.A_REVERSE)
			myscreen.refresh()
			mytext = get_text(myscreen,7,35)
			
	# This handles returning us back to the main menu or gives the user an option to run through again
	# It also handles giving the user a chance to exit the program
	exitflag = 0
	curses.noecho()
	while exitflag == 0:
		exitnow = myscreen.getch()
		if exitnow == ord('x') or exitnow == ord('X'):
			curses.echo()
			exitflag = 1
			myscreen.clear()
			myscreen.refresh()
			curses.endwin()
			sys.exit(0)
		if exitnow == ord('1'):
			curses.echo()
			exitflag = 1
			cdectobin(myscreen)
		if exitnow == ord('\n'):
			curses.echo()
			exitflag = 1
			return True

# This handles converting a decimal to hex string
def cdectohex(myscreen):
	running = 1
	optch = 0

	myscreen.clear()
	myscreen.border(0)
	#Set up the title bar
	subtitle(myscreen, 'UWL Number Converter', 'Decimal to Hexadecimal')
	
	# Set up the statusbar
	statusbar(myscreen, 'Type x followed by the return key to return to the main menu')

	# Set up the main screen
	myscreen.addstr(7, 4, "Enter a whole number: ")

	# Get the users input
	mytext = get_text(myscreen,7,35)

	while running == 1:
		if mytext == 'x' or mytext == 'X':
			myscreen.clear()
			myscreen.border(0)
			running = 0
			return True
		elif typetest.isInt(mytext):
			decno = mytext
			hexno = engine.dectohex(decno)
			mystring = decno + ' converted in to hexadecimal is: ' + hexno

			#Lets clear the input line and error line to save any confusion
			height,width = myscreen.getmaxyx()
			myscreen.addstr(7, 4, '')
			myscreen.hline(' ', (width-5))
			myscreen.addstr(9, 4, '')
			myscreen.hline(' ', (width-5))

			#Display the result of the conversion and what the user should do next
			convmessage(myscreen, mystring)
			statusbar(myscreen, 'x to exit | 1 to convert another | Press enter to return to the main menu')
			myscreen.refresh()
			running = 0
		else:
			myscreen.addstr(7,35, '')
			linelength = 15
			myscreen.hline(' ', linelength)
			myscreen.addstr(9,35,'Enter a valid number or x to exit', curses.A_REVERSE)
			myscreen.refresh()
			mytext = get_text(myscreen,7,35)

	exitflag = 0
	curses.noecho()
	while exitflag == 0:
		exitnow = myscreen.getch()
		if exitnow == ord('x') or exitnow == ord('X'):
			curses.echo()
			exitflag = 1
			myscreen.clear()
			myscreen.refresh()
			curses.endwin()
			sys.exit(0)
		if exitnow == ord('1'):
			curses.echo()
			exitflag = 1
			cdectohex(myscreen)
		if exitnow == ord('\n'):
			curses.echo()
			exitflag = 1
			return True

# This handles converting a binary to decimal string
def cbintodec(myscreen):
	running = 1
	optch = 0

	myscreen.clear()
	myscreen.border(0)
	#Set up the title bar
	subtitle(myscreen, 'UWL Number Converter', 'Binary to Decimal')
	
	# Set up the statusbar
	statusbar(myscreen, 'Type x followed by the return key to return to the main menu')

	# Set up the main screen
	prompt = "Enter a binary string [1's and 0's]: "
	inputpos = len(prompt) + 4
	myscreen.addstr(7, 4, prompt)

	# Get the users input
	mytext = get_text(myscreen,7,inputpos)

	while running == 1:
		if mytext == 'x' or mytext == 'X':
			myscreen.clear()
			myscreen.border(0)
			running = 0
			return True
		elif typetest.isBin(mytext):
			binno = mytext
			decno = engine.bintodec(binno)
			mystring = binno + ' converted in to decimal is: ' + str(decno)

			#Lets clear the input line and error line to save any confusion
			height,width = myscreen.getmaxyx()
			myscreen.addstr(7, 4, '')
			myscreen.hline(' ', (width-5))
			myscreen.addstr(9, 4, '')
			myscreen.hline(' ', (width-5))

			#Display the result of the conversion and what the user should do next
			convmessage(myscreen, mystring)
			statusbar(myscreen, 'x to exit | 1 to convert another | Press enter to return to the main menu')
			myscreen.refresh()
			running = 0
		else:
			myscreen.addstr(7,inputpos, '')
			linelength = 15
			myscreen.hline(' ', linelength)
			myscreen.addstr(9,inputpos,'Enter a valid binary number or x to exit', curses.A_REVERSE)
			myscreen.refresh()
			mytext = get_text(myscreen,7,inputpos)

	exitflag = 0
	curses.noecho()
	while exitflag == 0:
		exitnow = myscreen.getch()
		if exitnow == ord('x') or exitnow == ord('X'):
			curses.echo()
			exitflag = 1
			myscreen.clear()
			myscreen.refresh()
			curses.endwin()
			sys.exit(0)
		if exitnow == ord('1'):
			curses.echo()
			exitflag = 1
			cbintodec(myscreen)
		if exitnow == ord('\n'):
			curses.echo()
			exitflag = 1
			return True

# This handles converting a binary to hex string
def cbintohex(myscreen):
	running = 1
	optch = 0

	myscreen.clear()
	myscreen.border(0)
	#Set up the title bar
	subtitle(myscreen, 'UWL Number Converter', 'Binary to Hexadecimal')
	
	# Set up the statusbar
	statusbar(myscreen, 'Type x followed by the return key to return to the main menu')

	# Set up the main screen
	prompt = "Enter a binary string [1's and 0's]: "
	inputpos = len(prompt) + 4
	myscreen.addstr(7, 4, prompt)

	# Get the users input
	mytext = get_text(myscreen,7,inputpos)

	while running == 1:
		if mytext == 'x' or mytext == 'X':
			myscreen.clear()
			myscreen.border(0)
			running = 0
			return True
		elif typetest.isBin(mytext):
			binno = mytext
			hexno = engine.bintohex(binno)
			mystring = binno + ' converted in to hexadecimal is: ' + hexno

			#Lets clear the input line and error line to save any confusion
			height,width = myscreen.getmaxyx()
			myscreen.addstr(7, 4, '')
			myscreen.hline(' ', (width-5))
			myscreen.addstr(9, 4, '')
			myscreen.hline(' ', (width-5))

			#Display the result of the conversion and what the user should do next
			convmessage(myscreen, mystring)
			statusbar(myscreen, 'x to exit | 1 to convert another | Press enter to return to the main menu')
			myscreen.refresh()
			running = 0
		else:
			myscreen.addstr(7,inputpos, '')
			linelength = 15
			myscreen.hline(' ', linelength)
			myscreen.addstr(9,inputpos,'Enter a valid binary number or x to exit', curses.A_REVERSE)
			myscreen.refresh()
			mytext = get_text(myscreen,7,inputpos)

	exitflag = 0
	curses.noecho()
	while exitflag == 0:
		exitnow = myscreen.getch()
		if exitnow == ord('x') or exitnow == ord('X'):
			curses.echo()
			exitflag = 1
			myscreen.clear()
			myscreen.refresh()
			curses.endwin()
			sys.exit(0)
		if exitnow == ord('1'):
			curses.echo()
			exitflag = 1
			cbintohex(myscreen)
		if exitnow == ord('\n'):
			curses.echo()
			exitflag = 1
			return True

# This handles converting a hex to decimal string
def chextodec(myscreen):
	running = 1
	optch = 0

	myscreen.clear()
	myscreen.border(0)
	#Set up the title bar
	subtitle(myscreen, 'UWL Number Converter', 'Hexadecimal to Decimal')
	
	# Set up the statusbar
	statusbar(myscreen, 'Type x followed by the return key to return to the main menu')

	# Set up the main screen
	prompt = "Enter a hexadecimal string [0-9 and a-f]: "
	inputpos = len(prompt) + 4
	myscreen.addstr(7, 4, prompt)

	# Get the users input
	mytext = get_text(myscreen,7,inputpos)

	while running == 1:
		if mytext == 'x' or mytext == 'X':
			myscreen.clear()
			myscreen.border(0)
			running = 0
			return True
		elif typetest.isHex(mytext):
			hexno = mytext
			decno = engine.hextodec(hexno)
			mystring = hexno + ' converted in to decimal is: ' + str(decno)

			#Lets clear the input line and error line to save any confusion
			height,width = myscreen.getmaxyx()
			myscreen.addstr(7, 4, '')
			myscreen.hline(' ', (width-5))
			myscreen.addstr(9, 4, '')
			myscreen.hline(' ', (width-5))

			#Display the result of the conversion and what the user should do next
			convmessage(myscreen, mystring)
			statusbar(myscreen, 'x to exit | 1 to convert another | Press enter to return to the main menu')
			myscreen.refresh()
			running = 0
		else:
			myscreen.addstr(7,inputpos, '')
			linelength = 15
			myscreen.hline(' ', linelength)
			myscreen.addstr(9,inputpos,'Enter a valid hexadecimal number or x to exit', curses.A_REVERSE)
			myscreen.refresh()
			mytext = get_text(myscreen,7,inputpos)

	exitflag = 0
	curses.noecho()
	while exitflag == 0:
		exitnow = myscreen.getch()
		if exitnow == ord('x') or exitnow == ord('X'):
			curses.echo()
			exitflag = 1
			myscreen.clear()
			myscreen.refresh()
			curses.endwin()
			sys.exit(0)
		if exitnow == ord('1'):
			curses.echo()
			exitflag = 1
			chextodec(myscreen)
		if exitnow == ord('\n'):
			curses.echo()
			exitflag = 1
			return True

# Lastly this handles convering a hex string to a binary string
def chextobin(myscreen):
	running = 1
	optch = 0

	myscreen.clear()
	myscreen.border(0)
	#Set up the title bar
	subtitle(myscreen, 'UWL Number Converter', 'Hexadecimal to Binary')
	
	# Set up the statusbar
	statusbar(myscreen, 'Type x followed by the return key to return to the main menu')

	# Set up the main screen
	prompt = "Enter a hexadecimal string [0-9 and a-f]: "
	inputpos = len(prompt) + 4
	myscreen.addstr(7, 4, prompt)

	# Get the users input
	mytext = get_text(myscreen,7,inputpos)

	while running == 1:
		if mytext == 'x' or mytext == 'X':
			myscreen.clear()
			myscreen.border(0)
			running = 0
			return True
		elif typetest.isHex(mytext):
			hexno = mytext
			binno = engine.hextobin(hexno)
			mystring = hexno + ' converted in to hexadecimal is: ' + binno

			#Lets clear the input line and error line to save any confusion
			height,width = myscreen.getmaxyx()
			myscreen.addstr(7, 4, '')
			myscreen.hline(' ', (width-5))
			myscreen.addstr(9, 4, '')
			myscreen.hline(' ', (width-5))

			#Display the result of the conversion and what the user should do next
			convmessage(myscreen, mystring)
			statusbar(myscreen, 'x to exit | 1 to convert another | Press enter to return to the main menu')
			myscreen.refresh()
			running = 0
		else:
			myscreen.addstr(7,inputpos, '')
			linelength = 15
			myscreen.hline(' ', linelength)
			myscreen.addstr(9,inputpos,'Enter a valid hexadecimal number or x to exit', curses.A_REVERSE)
			myscreen.refresh()
			mytext = get_text(myscreen,7,inputpos)

	exitflag = 0
	curses.noecho()
	while exitflag == 0:
		exitnow = myscreen.getch()
		if exitnow == ord('x') or exitnow == ord('X'):
			curses.echo()
			exitflag = 1
			myscreen.clear()
			myscreen.refresh()
			curses.endwin()
			sys.exit(0)
		if exitnow == ord('1'):
			curses.echo()
			exitflag = 1
			chextobin(myscreen)
		if exitnow == ord('\n'):
			curses.echo()
			exitflag = 1
			return True

# This is our main function and is called from our converter
def main(running):
	# This sets up an internal running loop
	# It gets its running flag from our bootstrap code in converter
	while running == 1:
		# Create a new curses window
		myscreen = curses.initscr()
		# Lets clean the window so we have a blank canvas
		myscreen.clear()
		# Lets give the window a nice border
		myscreen.border(0)
		
		#Lets give the window a title
		winsize = myscreen.getmaxyx()
		wintitle = 'UWL Number converter'
		hpos = len(wintitle) // 2
		hpos = (winsize[1]//2) - hpos
		# The following few lines displays a menu for the user
		myscreen.addstr(2, hpos, wintitle)
		myscreen.addstr(4, 1, '')
		myscreen.hline('-', winsize[1]-2)
		myscreen.addstr(6, 2, "Please enter a number...")
		myscreen.addstr(7, 4, "1 - Decimal to Binary")
		myscreen.addstr(8, 4, "2 - Decimal to Hexadecimal")
		myscreen.addstr(9, 4, "3 - Binary to Decimal")
		myscreen.addstr(10, 4, "4 - Binary to Hexadecimal")
		myscreen.addstr(11, 4, "5 - Hexadecimal to Decimal")
		myscreen.addstr(12, 4, "6 - Hexadecimal to Binary")
		myscreen.addstr(13, 4, "X - Exit")
		myscreen.addstr(15, 4, "Enter your section: ")
		# Refresh the screen to display our menu
		myscreen.refresh()
		
		#Get the users input
		optch = myscreen.getkey()
		# And process the users input (this might be best replaced with
		# Sudo Switch case statement at a later date
		if optch == "x" or optch == "X":
			running = 0
		elif optch == "1":
			cdectobin(myscreen)
		elif optch == "2":
			cdectohex(myscreen)
		elif optch == "3":
			cbintodec(myscreen)
		elif optch == "4":
			cbintohex(myscreen)
		elif optch == "5":
			chextodec(myscreen)
		elif optch == "6":
			chextobin(myscreen)
			
	# If we exit our running loop we return the termal back to normal and exit
	curses.endwin()

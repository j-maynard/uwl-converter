#!/usr/bin/env python3
#import system functions to provide us with version information
import sys

# Before we do anything... Lets make sure we're running python 3.2 or above
# Get the system version and test to see if its below python 3
if sys.version_info[0] < 3:
	errormsg = "You need pyton version 3.2 or above to run this program"
	print(errormsg)
	#If the user doesn't meet the minimum specs exit here.
	sys.exit(2)
elif sys.version_info[0] == 3 and sys.version_info[1] < 2:
	errormsg = "You need pyton version 3.2 or above to run this program"
	print(errormsg)
	#If the user doesn't meet the minimum specs exit here.
	sys.exit(2)

import getopt
import typetest
import engine
import bulk
from guinteractive import *

# Check to see if curses is available
try:
	import cinteractivemode
	dos = 0
except:
	# If we're here it isn't... we must be on a Windows/DOS system or in Idle :(
	import interactivemode
	dos = 1

#Define help and version information
help = "\nUsage: " + sys.argv[0] + " [argument] [number] 	Normal usage\n\
   or: " + sys.argv[0] + " -i			Console interactive mode\n\
   or: " + sys.argv[0] + " -g			GUI mode\n\
   or: " + sys.argv[0] + " -d			DOS Simulated mode\n\
   or: " + sys.argv[0] + " -f [file]		Bulk file mode\n\
\n\
Notes: \n\
  * The number should always be specified as a whole number \n\
  * You can specify multiple argument/number pairs at the command line \n\
  * [argument]/[number] pairs will always override gui and interactive mode \n\
\n\
Arguments:\n \
  --db			Decimal to binary\n \
  --dh			Decimal to hex\n \
  --bd			Binary to decimal\n \
  --bh			Binary to hex\n \
  --hd			Hex to decimal\n \
  --hb			Hex to binary\n \
  -h or --help		Print Help (this message) and exit\n \
  -v or --version	Print version information and exit\n \
"
# This provides the program version information
version = sys.argv[0] + " Version 1.0 \n\
(c) Jamie Maynard 2013 \n\
Computer System Fundamentals Assignment 4"

# Before we can grab an input off the commandline
# Lets make sure IDLE isn't running
if str(sys.stdout).count('idlelib'):
	#Set an program flag that idle is running
	idlerunning = True
	# Print a warning to the user and provide a basic interactive mode
	print('IDLE DETECTED!!')
	print('I have detected that your are running this program in idle.')
	print('For a fuller experience please run from the commandline.')
	print('\nHow would you like to continue?\n')
	print('    1. Run in GUI mode')
	print('    2. Run in console interactive mode')
	print('    3. Display terminal help output')
	print('    4. Stop processing and exit')
	idlemode = input('Type a number and hit return: ')
	idlerun = 1
	#This while loop waits for the user to give us a valid input
	while idlerun == 1:
		if typetest.isInt(idlemode) and int(idlemode) > 0 and int(idlemode) < 5:
			idlerun = 0
		else:
			print('I\'m sorry I didn\'t understand your input')
			mode = input('Please chose an option between 1 and 4: ')
	#Pass the user off on to their choice and then exit the program		
	if int(idlemode) == 1:
		print('Switching to GUI mode... ')
		startgui()
	elif int(idlemode) == 2:
		print('Switching to Console Interactive mode…')
		interactivemode.main(running)
	elif int(idlemode) == 3:
		print('Now displaying terminal help... ')
		print(help)
	elif int(idlemode) == 4:
		print('Exiting…')
		
# If idle isn't running set the idlerunning flag to false and carry on as normal
else:
	idlerunning = False 

# Allow grabbing of the inputs from the commandline 
try:
	opts, args = getopt.gnu_getopt(sys.argv[1:],"hidgvf:",[
													"dosmode",
													"version",
													"file=",
													"decimal-binary=",
													"db=",
													"decimal-hex=",
													"dh=",
													"binary-decimal=",
													"bd=",
													"binary-hex=",
													"bh=",
													"hex-decimal=",
													"hd=",
													"hex-binary=",
													"hb="
													])
# Any problems or errors from the command line we print the help information
except getopt.GetoptError as err:
	print(help)
	sys.exit(2)

# Setup a few variables which are always used when the program runs
running = 1
interactive = 0
curses = 0
gui = 0

# Capture no arguments and print the help
# This should be changed to drop to interactive mode
if len(opts) == 0 and not idlerunning:
	print(help)
	sys.exit(0)

# This rather complex group of if statmenets sorts out the commandline arguments
# Some commandline arguments are processed within the for loop to allow multipual
# Argument number pairs to be processed before exiting.
for opt, arg in opts:
	# This prints the help information and then exits the program
	if opt == '-h':
		print(help)
		sys.exit(0)
		
	# This prints the version information and the exits the program
	elif opt in ("-v", "--version"):
		print(version)
		sys.exit(0)

	# This puts us in interactive mode
	elif opt == '-i':
		interactive = 1
		
	# This puts us in bulk file mode
	elif opt == '-f':
		print('Entering Bulk File mode...') #debug text
		bulk.main(arg)
		
	# This sets us in in decimal to binary mode 1
	elif opt in ("--db", "--decimal-binary"):
		# Grab the command line argument and
		# put it in decno
		decno = arg
		# Convert it and place the resultant binary in to binno
		binno = engine.dectobin(decno)
		# In the event of an error
		if binno == 'err':
			print("This isn't a valid decimal number")
		else:
			# Otherwise print the result 
			print(decno, 'converted to binary is:', binno)
			
		#This stops us entering interactive mode when we finish
		running = 0

	# This sets us in in decimal to hex mode 2
	elif opt in ("--dh", "--decimal-hex"):
		decno = arg
		print("Decimal to Hex Mode.  Number entered:", decno) #Debug text
		hexno = engine.dectohex(decno)
		
		if hexno == 'err':
			print("This isn't a valid hexadecimal number")
		else:
			print(decno, 'converted to hexadecimal is:', hexno)

		running = 0

	# This sets us in in binary to decimal mode 3
	elif opt in ("--bd", "--binary-decimal"):
		binno = arg
		print("Binary to Decimal Mode.  Number entered:", binno) #Debug text
		decno = engine.bintodec(binno)

		if decno == 'err':
			print("This isn't a valid binary number")
		else:
			print(binno, 'converted to decimal is:', decno)

		running = 0

	# This sets us in in binary to hex mode 4
	elif opt in ("--bh", "--binary-hex"):
		binno = arg
		print("Binary to Hex Mode.  Number entered:", binno) #Debug text

		hexno = engine.bintohex(binno)

		if hexno == 'err':
			print("This isn't a valid binary number")
		else:
			print(binno, 'converted to hexadecimal is:', hexno)

		running = 0

	# This sets us in in hex to decimal mode 5
	elif opt in ("--hd", "--hex-decimal"):
		hexno = arg
		print("Hex to Decimal Mode.  Number entered:", hexno) #Debug text

		decno = engine.hextodec(hexno)

		if decno == 'err':
			print("This isn't a valid hexadecimal number")
		else:
			print(hexno, 'converted in to decimal is:', decno)

		running = 0

	# This sets us in in hex to binary mode 6
	elif opt in ("--hb", "--hex-binary"):
		hexno = arg
		print("Hex to Binary Mode.  Number entered:", hexno) #Debug text

		binno = engine.hextobin(hexno)

		if binno == 'err':
			print("This isn't a valid hexadecimal number")
		else:
			print(hexno, 'converted in to binary is:', binno)

		running = 0

	# Again a second check to see if we want interactice mode
	elif opt in ('-i', "--interative"):
		running = 1
	
	# This simulates what happens when running on dos/windows system
	elif opt in ('-d', "--dosmode"):
		import interactivemode
		dos = 1
		running = 1
		
	# Again a second check to see if we're in interactice mode
	# In a future revision this needs to be updated to switch
	# to console mode if the GUI should fail for any reason
	elif opt in ('-g', "--gui"):
		gui = 1
		#try:
		startgui()
		#except:
		#	print('Theres a problem with gui mode switching to console mode')
		#	if dos == 0:
		#		cinteractivemode.main(running)
		#	else:
		#		print('Curses interactive mode not available.  Switching to console interactive mode')
		#		interactivemode.main(running)
		sys.exit(0)

	# If all else fails... display help and exit
	# This can be changed to switch to interactive mode testing for a GUI and then curses
	else:
		print(help)
		sys.exit(0)

#If we've made it this far check to make sure we're nor running in idle			
if not idlerunning:
	# Next see if we just need to exit
	if running == 0:
		sys.exit(0)
	# If not carry on in to normal interactive mode
	if dos == 0:
		cinteractivemode.main(running)
	else:
		print('Curses interactive mode not available.  Switching to console interactive mode')
		interactivemode.main(running)

#!/usr/bin/env python3
import typetest

# This function takes a decimal number and converts it to binary
# This function only returns the binary number not the route there
def dectobin(decno):
	#First stop... check to make sure our number is an integer
	if not typetest.isInt(decno):
		# If its not an integer return an error and exit this function
		return 'err'

	# Otherwise convert our decimal number to binary number
	# First check to make sure its not a 0
	if decno == 0:
		# If it is set our binary number to 0 and return 0
		binno = 0
		return binno
	else:
		binno = ""
		while int(decno) > 0:
			remainder = int(decno) % 2
			decno = int(decno) // 2
			binno = str(remainder) + binno

		# Return our binary number string
		return binno

# This function takes a decimal number and convert it to hexadecimal
# This function only returns the hexadecimal number
def dectohex(decno):
	# First stop... check to make sure our number is an integer
	if not typetest.isInt(decno):
		return 'err'
	# Use the hex function to convert the decimal number to a hexadecimal
	# the [2:] make sure we don't the 0x at the start of the string
	hexno = hex(int(decno))[2:]
	return hexno

# This function takes a binary number and converts it to decimal
# This function only returns the answer
def bintodec(binno):
	# First stop check to make sure our number is a binary string
	if not typetest.isBin(binno):
		# If this isn't a binary string return an error
		return 'err'

	# Use our original bin to decimal code from CSF element 4
	# There is a shorter alternative provided by python but this works
	# just as well.
	decno = 0
	exponent = len(binno) - 1
	for digit in binno:
		decno = decno + int(digit) * 2 ** exponent
		exponent = exponent - 1
	# Return our decimal number
	return decno

# This function converts our binary number to a hexadecimal number
# It does this by first converting it to a decimal number and then
# converting to a hexadecimal number which it returns.
def bintohex(binno):
	# First stop check to make sure our number is a binary string
	if not typetest.isBin(binno):
		# If this isn't a binary string return an error
		return 'err'

	# Convert to decimal
	decno = int(binno, 2)

	# Convert to hex
	# The [2:] makes sure we don't include the 0x at the start of
	# the return variable
	hexno = hex(decno)[2:]

	#return our hexadecimal number
	return hexno

# This function converts our hexadecimal string to a decimal number
# and then returns the decimal number
def hextodec(hexno):
	# First stop check to make sure our number is a hexadecimal string
	if not typetest.isHex(hexno):
		# If this isn't a hexadecimal string return an error
		return 'err'

	# No erros so lets do a straight up conversion
	# using the ints base function to convert base 16 to base 10 (integer)
	decno = int(hexno, 16)
	
	# Now lets return our decimal number
	return decno

# Thsi function converts our hexadecimal string to a binary string
# It does this by first converting it to a decimal and then to a binary
# returning the binary string
def hextobin(hexno):
	# First stop check to make sure our number is a hexadecimal string
	if not typetest.isHex(hexno):
		# If this isn't a hexadecimal string return an error
		return 'err'

	# Convert to decimal
	decno = int(hexno, 16)

	# Conver to binary
	binno = dectobin(decno)

	# Return our binary string
	return binno

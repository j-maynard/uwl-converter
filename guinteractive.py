#!/usr/bin/env python3

#Import our required modules
from tkinter import *
import tkinter.messagebox
import sys
import typetest
import engine

# Creat the version information string
# This is used in the about diaglogue box
version = sys.argv[0] + " Version 1.0 \n\
(c) Jamie Maynard 2013 \n\
Computer System Fundamentals Assignment 4"

# This class defines our GUI application
# and handles all of its functions internally
class App:
	
	# This initializes our class
	def __init__(self, root, idlerunning):
		self.root = root
		self.idlerunning = idlerunning

		# Call start to initialize to create the UI elemets
		self.start()
		
	# This sets up our various GUI elements, menubars and keybindings
	def start(self):
		self.root.title("UWL Number Converter")
		
		# Turn off tear off menus
		self.root.option_add('*tearOff', FALSE)
		# Create a menu bar
		# File Menu
		top = Menu(self.root)
		file = Menu(top)
		file.add_command(label='Quit',command=self.close_cmd, accelerator="Ctrl+Q")
		top.add_cascade(label="File",menu=file)
		# Edit Menu
		edit = Menu(top)
		edit.add_command(label="Copy Output",command=self.clipfunc, accelerator="Ctrl+Shift+C")
		edit.add_command(label="Paste Input",command=self.pastefunc, accelerator="Ctrl+Shift+V")
		top.add_cascade(label="Edit",menu=edit)
		
		# Set up the matching keybindings to the menus
		self.root.bind_all('<Control-Shift-Q>', self.close_cmd)
		self.root.bind_all('<Control-q>', self.close_cmd)
		self.root.bind_all('<Control-Shift-C>', self.clipfunc)
		self.root.bind_all('<Control-Shift-V>', self.pastefunc)
		
		# Help Menu
		help = Menu(self.root)
		help = Menu(top)
		help.add_command(label='About',command=self.about)
		top.add_cascade(label="Help",menu=help)
		
		# Add the menubar
		self.root.config(menu=top)
		
		# Creat a label
		# create a variable with text
		label01 = "Input Number:"
		# put "label01" in "self.root" which is the window/frame
		# then, put in the first row (row=0) and in the 2nd column (column=1), align it to "West"/"W"
		Label(self.root, text=label01).grid(row=0, column=0, sticky=W)

		# Create an input textbox
		self.number = Entry(self.root)
		self.number["width"] = 40
		self.number.focus_set()
		self.number.grid(row=0,column=1,columnspan=2)

		# initialize a variable to store the selected value of the radio buttons
		# Set a default so that button is selected by default
		# In this case decimal to binary or db
		self.func_var = StringVar()
		self.func_var.set("db")

		# place the Radiobuttons
		dbradio = Radiobutton(self.root, text="Dec to Bin", variable=self.func_var, value="db").grid(row=1, column=0)
		dhradio = Radiobutton(self.root, text="Dec to Hex", variable=self.func_var, value="dh").grid(row=1, column=1)
		bdradio = Radiobutton(self.root, text="Bin to Dec", variable=self.func_var, value="bd").grid(row=1, column=2)
		bhradio = Radiobutton(self.root, text="Bin to Hex", variable=self.func_var, value="bh").grid(row=2, column=0)
		hdradio = Radiobutton(self.root, text="Hex to Dec", variable=self.func_var, value="hd").grid(row=2, column=1)
		hbradio = Radiobutton(self.root, text="Hex to Bin", variable=self.func_var, value="hb").grid(row=2, column=2)

		# now for a button or three
		self.copybtn = Button(self.root, text="Copy to Input", command=self.copyfunc)
		self.copybtn.grid(row=3, column=0)
		self.submit = Button(self.root, text="Convert", command=self.convert)
		self.submit.grid(row=3, column=1)
		self.clipbtn = Button(self.root, text="Copy to Clipboard", command=self.clipfunc)
		self.clipbtn.grid(row=3, column=2)
		
		# CREATE A TEXT/LABEL
		# create a variable with text
		label02 = "Output"
		# put "label02" in "self.root" which is the window/frame
		# then, put in the first row (row=0) and in the 2nd column (column=1), align it to "West"/"W"
		Label(self.root, text=label02).grid(row=4, column=0, sticky=W)

		# CREATE A TEXTBOX
		self.output = Entry(self.root)
		self.output["width"] = 40
		self.output.grid(row=4,column=1,columnspan=2)

	# This just acts as a switch to route numbers to their functions and expects a number string
	# in return
	def convert(self):
		# These lines of code are for debugging purposes only
		#print('The number is ', self.number.get(), ' Function is ', self.func_var.get())
		#outputstring = 'The number is ' + self.number.get() + ' Function is ' + self.func_var.get()

		#Lets move out needed variables in to local variables
		func = self.func_var.get()
		myno = self.number.get()

		# If Decimal to binary
		if func == 'db':
			# Test to make sure we have a valid number
			if typetest.isInt(myno):
				outputstring = engine.dectobin(myno)
			else:
				# If not update our output string
				outputstring = 'Not a valid whole number'
				# and raise a dialogue box
				tkinter.messagebox.showinfo("Bad Input", outputstring)
		# If Decimal to hex
		elif func == 'dh':
			if typetest.isInt(myno):
				outputstring = engine.dectohex(myno)
			else:
				outputstring = 'Not a valid whole number'
				tkinter.messagebox.showinfo("Bad Input", outputstring)
		# If Binary to decimal
		elif func == 'bd':
			if typetest.isBin(myno):
				outputstring = engine.bintodec(myno)
			else:
				outputstring = 'Not a valid binary number'
				tkinter.messagebox.showinfo("Bad Input", outputstring)
		# If Binary to hex
		elif func == 'bh':
			if typetest.isBin(myno):
				outputstring = engine.bintohex(myno)
			else:
				outputstring = 'Not a valid binary number'
				tkinter.messagebox.showinfo("Bad Input", outputstring)
		# If Hex to Decimal
		elif func == 'hd':
			if typetest.isHex(myno):
				outputstring = engine.hextodec(myno)
			else:
				outputstring = 'Not a valid hexadecimal number'
				tkinter.messagebox.showinfo("Bad Input", outputstring)
		# If Hex to Binary
		elif func == 'hb':
			if typetest.isHex(myno):
				outputstring = engine.hextobin(myno)
			else:
				outputstring = 'Not a valid hexadecimal number'
				tkinter.messagebox.showinfo("Bad Input", outputstring)

		#THIS IS THE OUTPUT CODE... This deletes the contents of the output box and replaces it.
		self.output.delete(0,END)
		self.output.insert(0,outputstring)
	
	# This internal function handles quitting safely	
	def close_cmd(self, event=None):
		# Check to see if Idle is running
		if self.idlerunning:
			# If it is don't exit, just destroy the window
			self.root.destroy()
		else:
			# Otherwise we just exit here
			sys.exit()
	
	# A simple function to display an about message
	def about(self, event=None):
		tkinter.messagebox.showinfo("About...", version)
	
	# This handles copying the output back up to the input		
	def copyfunc(self, event=None):
		inputstring = self.output.get()
		self.number.delete(0,END)
		self.number.insert(0,inputstring)
	
	# This handles copying the out to the clipboard
	def clipfunc(self, event=None):
		copystring = self.output.get()
		self.root.clipboard_clear()
		self.root.clipboard_append(copystring)
		
	# This function handles pasting the clipboard to the input
	def pastefunc(self, event=None):
		inputstring = self.root.selection_get(selection='CLIPBOARD')
		self.number.delete(0,END)
		self.number.insert(0,inputstring)

# Call this from our converter to start our gui application
def startgui(idlerunning=False):
	root = Tk()
	app = App(root, idlerunning)
	root.mainloop()
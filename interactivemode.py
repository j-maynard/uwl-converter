#!/usr/bin/env python3 

import sys
import typetest
import engine

# Code implements switch and case statments and is taken from
# http://stackoverflow.com/questions/60208/replacements-for-switch-statement-in-python
class switch( object ):
    value = None
    def __new__( class_, value ):
        class_.value = value
        return True
# Code implements switch and case statments and is taken from
# http://stackoverflow.com/questions/60208/replacements-for-switch-statement-in-python
def case( *args ):
    return any( ( arg == switch.value for arg in args ) )
    
# This handles converting a decimal to a binary
def idectobin():
	# Ask the user for a number or give them the option to return to the main menu
	decno = input('Please enter a whole number(or m to return to the main menu): ')
	# If they choose to return to the menu jsut return true to leave the function
	if decno == 'm' or decno == 'M':
		return True
		
	# Test to make sure the user has given us a valid number
	while not typetest.isInt(decno):
		print('Thats not a valid number')
		decno = input('Please enter a whole number(or m to return to the main menu): ')
		if decno == 'm' or decno == 'M':
			return True
	# If the user has given us a valid number process it
	binno = engine.dectobin(decno)
	# and display the result
	print('\n', decno, 'coverted to binary is:', binno)
	# Advise the user to return to the main menu or given them the chance
	# To exit the program
	choice = input('\nPress return to go back to the main menu or x to exit: ')
	if choice == 'x' or choice == 'X':
		iexit()

	return True
	
# This handles converting a decimal to a hex
def idectohex():
	decno = input('Please enter a whole number(or m to return to the main menu): ')
	if decno == 'm' or decno == 'M':
		return True

	while not typetest.isInt(decno):
		print('Thats not a valid number')
		decno = input('Please enter a whole number(or m to return to the main menu): ')
		if decno == 'm' or decno == 'M':
			return True

	hexno = engine.dectohex(decno)
	print('\n', decno, 'coverted to hexadecimal is:', hexno)
	choice = input('\nPress return to go back to the main menu or x to exit: ')
	if choice == 'x' or choice == 'X':
		iexit()

	return True

# This handles converting a binary to a decimal
def ibintodec():
	binno = input('Please enter a binary string [1 and 0\'s] (or m to return to the main menu): ')
	if binno == 'm' or binno == 'M':
		return True

	while not typetest.isBin(binno):
		print('Thats not a valid binary number')
		binno = input('Please enter a binary string [1 and 0\'s] (or m to return to the main menu): ')
		if decno == 'm' or decno == 'M':
			return True

	decno = engine.bintodec(binno)
	print('\n', binno, 'coverted to decimal is:', decno)
	choice = input('\nPress return to go back to the main menu or x to exit: ')
	if choice == 'x' or choice == 'X':
		iexit()

	return True

# This handles converting a binary to a hex
def ibintohex():
	binno = input('Please enter a binary string [1 and 0\'s] (or m to return to the main menu): ')
	if binno == 'm' or binno == 'M':
		return True

	while not typetest.isBin(binno):
		print('Thats not a valid binary number')
		binno = input('Please enter a binary string [1 and 0\'s] (or m to return to the main menu): ')
		if decno == 'm' or decno == 'M':
			return True

	hexno = engine.bintohex(binno)
	print('\n', binno, 'coverted to hexadecimal is:', hexno)
	choice = input('\nPress return to go back to the main menu or x to exit: ')
	if choice == 'x' or choice == 'X':
		iexit()

	return True

# This handles converting a hex to a decimal
def ihextodec():
	hexno = input('Please enter a hexadecimal string [0 -9 and a -f] (or m to return to the main menu): ')
	if hexno == 'm' or hexno == 'M':
		return True

	while not typetest.isHex(hexno):
		print('Thats not a valid hexadecimal number')
		hexno = input('Please enter a decimal string [0 -9 and a -f] (or m to return to the main menu): ')
		if hexno == 'm' or hexno == 'M':
			return True

	decno = engine.hextodec(hexno)
	print('\n', hexno, 'coverted to hexadecimal is:', decno)
	choice = input('\nPress return to go back to the main menu or x to exit: ')
	if choice == 'x' or choice == 'X':
		iexit()

	return True

# This handles converting a hex to a binary
def ihextobin():
	hexno = input('Please enter a hexadecimal string [0 -9 and a -f] (or m to return to the main menu): ')
	if hexno == 'm' or hexno == 'M':
		return True

	while not typetest.isHex(hexno):
		print('Thats not a valid hexadecimal number')
		hexno = input('Please enter a decimal string [0 -9 and a -f] (or m to return to the main menu): ')
		if hexno == 'm' or hexno == 'M':
			return True

	binno = engine.hextobin(hexno)
	print('\n', hexno, 'coverted to binary is:', binno)
	choice = input('\nPress return to go back to the main menu or x to exit: ')
	if choice == 'x' or choice == 'X':
		iexit()

	return True

# This function handles exiting the program
# It checks to make sure we're not running idle
def iexit(idlerunning):
	if not idlerunning:
		# If we're not running idle... just exit here 
		print('Exiting... ') 
		sys.exit(0)
	else:
		# Other wise we return and hope the script ends here
		return True

# This is our main function called
# from our converter bootstrap code
# It advises us if we're running in idle
def main(running, idlerunning=False):
	# Advise the user we're entering interactive mode
	print('Entering interactive mode...')

	# Start a program loop
	while running == 1:
		# Display a menu for the user
		print('Please choose a function:')
		print('1 for Decimal to Binary conversions')
		print('2 for Decimal to Hexadecimal conversions')
		print('3 for Binary to Decimal conversions')
		print('4 for Binary to Hexadecimal conversions')
		print('5 for Hexadecimal to Decimal conversions')
		print('6 for Hexadecimal to Binary conversions')
		print('X to Exit')
		choice = input('Please enter your choice: ')
		
		# This case switch quickly handles the users
		# choice switching to the right function
		# before returning to our loop until the user exits
		while switch (choice):
			if case('1'):
				idectobin()
				break
			if case('2'):
				idectohex()
				break
			if case('3'):
				ibintodec()
				break
			if case('4'):
				ibintohex()
				break
			if case('5'):
				ihextodec()
				break
			if case('6'):
				ihextobin()
				break
			if case('x', 'X', 'exit', 'EXIT'):
				iexit(idlerunning)
				running = 0
				break
			# If the user doesn't provide a valid option
			# advise the user and return to the top of the loop
			print('Thats not a valid option please choose again')
			break

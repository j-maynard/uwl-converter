#!/usr/bin/env python3

#The following function is used to make sure the variable passed is an integer
def isInt(s):
    try: 
        int(s)
        return True
    except ValueError:
        return False

# The following function is used to make sure the fariable passed is a valid hex number
def isHex(s):
	try:
		int(s, 16)
		return True
	except ValueError:
		return False

# The following function is used to make sure the variable passed is a valid binary number
def isBin(s):
	try:
		int(s, 2)
		return True
	except ValueError:
		return False